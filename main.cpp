#include <QApplication>
#include "xineconfigwidget.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    XineConfigWidget main(0);

    main.show();
    app.exec();
}
