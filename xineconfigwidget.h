#ifndef XINECONFIGWIDGET_H
#define XINECONFIGWIDGET_H

#include <QDialog>
#include <QGroupBox>
#include <QMap>

#include "xineconfigentryiterator.h"

class XineConfigWidget : public QDialog
{
    Q_OBJECT

public:
    XineConfigWidget(QWidget *parent);
    ~XineConfigWidget();

private slots:
    void handleChange(int);
    void handleChange(QString);
    void saveConfig();

private:
    XineConfigEntryIterator *m_iterator;
    xine_t *m_xine;
    QList<QWidget*> m_items;
};

#endif
