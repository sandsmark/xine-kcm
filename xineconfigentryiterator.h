#ifndef XINECONFIGENTRYITERATOR_H
#define XINECONFIGENTRYITERATOR_H

#include <xine.h>

class XineConfigEntryIterator {
public:
  XineConfigEntryIterator( xine_t *xine ) : m_xine( xine ) { m_valid = xine_config_get_first_entry( m_xine, &m_entry ); }
  inline XineConfigEntryIterator &operator++() { m_valid = xine_config_get_next_entry( m_xine, &m_entry ); return *this; }
  inline xine_cfg_entry_t *operator*() { return m_valid ? &m_entry : 0; }

private:
  xine_t *m_xine;
  xine_cfg_entry_t m_entry;
  bool m_valid;
};

#endif
