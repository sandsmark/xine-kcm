#include <QComboBox>
#include <QCheckBox>
#include <QDebug>
#include <QVBoxLayout>
#include <QFile>
#include <QLabel>
#include <QLineEdit>
#include <QSettings>
#include <QSpinBox>
#include <QScrollArea>
#include <QSpacerItem>
#include <QTabWidget>
#include <QGroupBox>

#include "xineconfigwidget.h"

XineConfigWidget::XineConfigWidget(QWidget *parent) : QDialog(parent)
{
    QTabWidget *tabWidget = new QTabWidget(this);

    setLayout(new QVBoxLayout());
    layout()->addWidget(tabWidget);

    m_xine = xine_new();

    QSettings cg("kde.org", "Phonon-Xine");
    xine_config_load(m_xine, (QFile::encodeName(cg.fileName())).constData());
    xine_init(m_xine);

    xine_video_port_t *nullVideoPort = xine_open_video_driver(m_xine, "auto", XINE_VISUAL_TYPE_NONE, 0);
    xine_post_t *deinterlacer = xine_post_init(m_xine, "tvtime", 1, 0, &nullVideoPort);

    xine_audio_port_t *nullAudioPort = xine_open_audio_driver(m_xine, "auto", 0);

    QStringList key;
    QString tab, category, name;
    QWidget *item;
    QMap<QString, QGroupBox*> categories;
    QMap<QString, QWidget*> tabs;

    for (XineConfigEntryIterator it(m_xine); *it; ++it) {
        key = QString::fromUtf8((*it)->key).split(".");

        if (key.length() < 3) // some misc shit, /care
            continue;

        tab = key[0];
        category = key[1];
        name = key[2];

        item = new QWidget();
        item->setObjectName((*it)->key);
        item->setLayout(new QHBoxLayout());
        item->layout()->addWidget(new QLabel(name));

        if (!tabs.contains(tab)) {
            QWidget *tabPage = new QWidget();
            tabs[tab] = tabPage;
            tabPage->setLayout(new QVBoxLayout());

            QScrollArea *scrollArea = new QScrollArea();
            scrollArea->setWidget(tabPage);
            scrollArea->setWidgetResizable(true);
            tabWidget->addTab(scrollArea, tab);
        }

        if (!categories.contains(category)) {
            QGroupBox *categoryGroup = new QGroupBox();
            categoryGroup->setLayout(new QVBoxLayout());
            categoryGroup->setTitle(category);
            categories[category] = categoryGroup;
            tabs[tab]->layout()->addWidget(categoryGroup);
        }

        switch ((*it)->type) {
        case XINE_CONFIG_TYPE_STRING: {
            QLineEdit *lineedit = new QLineEdit((*it)->str_value);
            item->layout()->addWidget(lineedit);

            connect(lineedit, SIGNAL(textChanged(QString)), this, SLOT(handleChange(QString)));
            break;
        }
        case XINE_CONFIG_TYPE_ENUM: {
            QComboBox *box = new QComboBox();
            box->setObjectName((*it)->key);

            for (int i=0; (*it)->enum_values[i]; i++)
                box->insertItem(i, (*it)->enum_values[i]);

            item->layout()->addWidget(box);

            connect(box, SIGNAL(currentIndexChanged(QString)), this, SLOT(handleChange(QString)));
            break;
        }
        case XINE_CONFIG_TYPE_RANGE:
        case XINE_CONFIG_TYPE_NUM: {
            QSpinBox *spinbox = new QSpinBox();
            spinbox->setObjectName((*it)->key);
            spinbox->setMinimum(qMin((*it)->num_value, (*it)->range_min));
            spinbox->setMaximum(qMax((*it)->num_value, (*it)->range_max));
            spinbox->setValue((*it)->num_value);

            item->layout()->addWidget(spinbox);

            connect(spinbox, SIGNAL(valueChanged(int)), this, SLOT(handleChange(int)));
            break;
        }
        case XINE_CONFIG_TYPE_BOOL: {
            QCheckBox *checkbox = new QCheckBox();
            checkbox->setObjectName((*it)->key);
            checkbox->setChecked((*it)->num_value);
            checkbox->setToolTip((*it)->help);

            item->layout()->addWidget(checkbox);

            connect(checkbox, SIGNAL(stateChanged(int)), this, SLOT(handleChange(int)));
            break;
        }
        default:
            ;
        }
        categories[category]->layout()->addWidget(item);
    }

    foreach(QWidget *page, tabs) {
        page->layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Maximum, QSizePolicy::Expanding));
    }

    xine_post_dispose(m_xine, deinterlacer);
    xine_close_video_driver(m_xine, nullVideoPort);
    xine_close_audio_driver(m_xine, nullAudioPort);
}

XineConfigWidget::~XineConfigWidget()
{
    xine_exit(m_xine);
}

void XineConfigWidget::handleChange(int value)
{
    xine_cfg_entry_t entry;

    const QString entryName = sender()->objectName(); // The name of the config entry is stored as object name
    xine_config_lookup_entry(m_xine, entryName.toLocal8Bit(), &entry);
    entry.num_value = value;
    xine_config_update_entry(m_xine, &entry);
}

void XineConfigWidget::handleChange(QString value)
{
    xine_cfg_entry_t entry;

    const QString entryName = sender()->objectName();
    xine_config_lookup_entry(m_xine, entryName.toLocal8Bit(), &entry);
    entry.str_value = value.toLocal8Bit().data();
    xine_config_update_entry(m_xine, &entry);
}

void XineConfigWidget::saveConfig()
{
    QSettings cg("kde.org", "Phonon-Xine");
    xine_config_save(m_xine, (QFile::encodeName(cg.fileName())).constData());
}
